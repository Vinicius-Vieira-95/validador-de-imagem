package com.validadordeimagem.vini.controllers;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.validadordeimagem.vini.model.Imagem;
import com.validadordeimagem.vini.services.ImagemService;

@RestController
@RequestMapping(name = "/imagens")
public class ImagemController {
	
    @Autowired
	private ImagemService imagemService;

    @PostMapping
    public ResponseEntity<Imagem> salvar(@RequestBody Imagem imagem, @RequestPart MultipartFile file) {
    	return ResponseEntity.ok().body(new Imagem()); //imagemService.salvar(imagem, file);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Resource> buscarPorId(@PathVariable("id") Long id) {

        var file = imagemService.buscarPorId(id);

        if(file == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

        ByteArrayResource resource = new ByteArrayResource(Base64.decodeBase64(file.toString()));

		return  ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
        .contentLength(resource.contentLength()).header(HttpHeaders.CONTENT_DISPOSITION, 
        ContentDisposition.attachment().filename("img.png").build().toString()).body(resource);
    }

    public void deletar(Long id) {
    	imagemService.deletar(id);
    }
}
