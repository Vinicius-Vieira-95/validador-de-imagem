package com.validadordeimagem.vini.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.validadordeimagem.vini.model.Imagem;

public interface ImagemRepository extends JpaRepository<Imagem, Long>{

}
