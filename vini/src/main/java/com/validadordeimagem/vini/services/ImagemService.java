package com.validadordeimagem.vini.services;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.validadordeimagem.vini.model.Imagem;
import com.validadordeimagem.vini.repository.ImagemRepository;

import jakarta.transaction.Transactional;

@Service
public class ImagemService {
    
    @Autowired
    private ImagemRepository imagemRepository;


    @Transactional
    public Imagem salvar(Imagem imagem, MultipartFile file) {

        try{
            imagem.setImg(file.getBytes());
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return imagemRepository.save(imagem);
    }

    public byte[] buscarPorId(Long id) {
        try {
            var img = imagemRepository.getReferenceById(id);
            return Base64.getEncoder().encode(img.getImg());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deletar(Long id) {
        imagemRepository.deleteById(id);
    }
}
